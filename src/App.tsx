import './styles.css';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import {
    AddPostForm,
    Entities,
    Generator,
    Landing,
    Layout,
} from '@homework-task/components';

import { data } from './schemas/Generator.schema';
import { UsersSchema } from './schemas/User.schema';

const queryClient = new QueryClient();

function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <Layout key={'layout'}>
                <Landing key={'landing'} />
                <Entities
                    key={'entities'}
                    urlExtension="users"
                    schema={UsersSchema}
                />
                <AddPostForm key={'addPostForm'} />
                <Generator key={'generator'} data={data} />
            </Layout>
        </QueryClientProvider>
    );
}

export default App;
