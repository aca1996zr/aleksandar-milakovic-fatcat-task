import { z } from 'zod';

const UserSchema = z.object({
    id: z.number(),
    name: z.string(),
    username: z.string(),
    email: z.string().email(),
    phone: z.string(),
});

export const UsersSchema = z.array(UserSchema);
