import { z } from 'zod';

export const FormValidatorSchema = z.object({
    title: z
        .string()
        .min(1, 'This field is required')
        .max(10, { message: 'Maximum number of characters is 10' }),
    body: z
        .string()
        .min(1, 'This field is required')
        .max(50, { message: 'Maximum number of characters is 50' }),
});
