import { SectionType } from '@homework-task/components';

export const data: Array<SectionType> = [
    {
        type: 'layoutSection',
        props: {
            title: 'Welcome to Our Site!',
            items: [{ description: 'Explore our features', title: 'Features' }],
            images: ['header-image.jpg'], // Assume this is a path to an image
            children: [],
        },
        components: [
            {
                type: 'componentHero',
                props: {
                    title: 'Hero Title: Empowering Your Business',
                    items: [
                        {
                            description:
                                'Leading solutions for modern businesses',
                            title: 'Innovative Solutions',
                        },
                    ],
                    image: '/media/landing/hero.svg',
                    images: [],
                    children: [],
                },
            },
        ],
    },
    {
        type: 'layoutSection',
        props: {
            title: 'Our Products',
            items: [
                {
                    description: 'Check out our wide range of products',
                    title: 'Product Range',
                },
            ],
            images: ['products-section.jpg'],
            children: [],
        },
        components: [
            {
                type: 'componentItemsShowcase',
                props: {
                    title: 'Featured Products',
                    items: [
                        {
                            description:
                                'Product 1: The next generation gadget',
                            title: 'Gadget Pro X',
                        },
                        {
                            description: 'Product 2: Enhance your productivity',
                            title: 'Smart Office Kit',
                        },
                    ],
                    images: ['/media/cats/cat_1.png', '/media/cats/cat_2.png'],
                    children: [],
                },
            },
            {
                type: 'componentTrustBar',
                props: {
                    title: 'Trusted by Thousands',
                    items: [
                        {
                            description:
                                'Serving over 10,000 businesses worldwide',
                            title: 'Trusted Partner',
                        },
                    ],
                    images: [
                        '/media/cats/cat_1.png',
                        '/media/cats/cat_2.png',
                        '/media/cats/cat_3.png',
                    ],
                    children: [],
                },
            },
        ],
    },
];
