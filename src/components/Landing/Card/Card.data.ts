export type CardType = {
    title: string;
    text: string;
    link: string;
};
