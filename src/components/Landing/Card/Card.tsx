import { clsx } from 'clsx';

import { CardType } from './Card.data';

export const Card = ({ title, text, link }: CardType) => {
    return (
        <div
            className={clsx(
                'flex',
                'flex-col',
                'gap-4',
                'p-4',
                'bg-white',
                'py-6',
                'px-4',
                'rounded-2xl'
            )}
        >
            <h2
                key={title}
                className={clsx(
                    'text-black',
                    'text-2xl',
                    'leading-normal',
                    'font-medium'
                )}
            >
                {title}
            </h2>
            <p
                key={text}
                className={clsx('text-gray80', 'text-base', 'leading-relaxed')}
            >
                {text}
            </p>
            <a
                key={link}
                href={link}
                className={clsx(
                    'mt-auto',
                    'flex',
                    'items-center',
                    'text-primary',
                    'gap-2.5'
                )}
            >
                Read more
                <img key={'img'} src="/media/landing/arrow-purple.svg" alt="" />
            </a>
        </div>
    );
};
