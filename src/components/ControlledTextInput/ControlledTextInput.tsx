import { useFormContext, Controller } from 'react-hook-form';

import { TextInput, TextInputProps } from '../TextInput';

interface ConterolledTextInputProps extends TextInputProps {
    name: string;
}

export const ControlledTextInput = ({
    name,
    ref,
    ...props
}: ConterolledTextInputProps) => {
    const { control } = useFormContext();

    return (
        <Controller
            name={name}
            control={control}
            defaultValue={''}
            render={({ field, fieldState }) => {
                const error = fieldState.error?.message;
                return (
                    <TextInput
                        error={error ?? ''}
                        {...props}
                        {...field}
                        ref={ref}
                    />
                );
            }}
        />
    );
};
