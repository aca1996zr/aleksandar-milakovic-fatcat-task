import { Item } from '@homework-task/models/data';

export type ItemsShowcaseProps = {
    items: Item[];
};
export const ItemsShowcase = ({ items }: ItemsShowcaseProps) => {
    return (
        <div className="flex justify-center items-center">
            <div
                key={'items-wrapper'}
                className="grid grid-cols-2 gap-8 w-8/12"
            >
                {items.map((item) => (
                    <div key={item.title} className="flex flex-col gap-2">
                        <img
                            key={item.image}
                            src="/media/checkmark.jpg"
                            width={25}
                            alt="Checkmark"
                        />
                        <div key={item.title} className="text-2xl font-bold">
                            {item.title}
                        </div>
                        <p key={item.description} className="">
                            {item.description}
                        </p>
                    </div>
                ))}
            </div>
        </div>
    );
};
