import { clsx } from 'clsx';

import { Button } from '@homework-task/components/Button';

type Card = {
    title: string;
    image: string;
    description: string;
    background: string;
    onClick: () => void;
    buttonText: string;
};

type CardsProps = {
    cards: Card[];
};

export const Cards = ({ cards }: CardsProps) => {
    return (
        <div className={clsx('flex', 'justify-center', 'items-center')}>
            <div
                key={'wrapper'}
                className={clsx('grid', 'grid-cols-2', 'gap-8', 'w-8/12')}
            >
                {cards.map((card) => (
                    <div
                        key={card.title}
                        className={clsx('rounded-md', 'p-8', card.background)}
                    >
                        <img
                            key={card.image}
                            src={card.image}
                            alt="Icon"
                            width="120"
                        />
                        <div key={'card-wrapper'} className="my-8">
                            <div
                                key={card.title}
                                className={clsx(
                                    'text-2xl',
                                    'font-bold',
                                    'mb-2'
                                )}
                            >
                                {card.title}
                            </div>
                            <div
                                key={card.description}
                                className={clsx('text-xl')}
                            >
                                {card.description}
                            </div>
                        </div>
                        <Button key={card.buttonText} onClick={card.onClick}>
                            {card.buttonText}
                        </Button>
                    </div>
                ))}
            </div>
        </div>
    );
};
