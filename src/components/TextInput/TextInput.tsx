import { ComponentProps } from 'react';

import { clsx } from 'clsx';

export interface TextInputProps extends ComponentProps<'input'> {
    error?: string;
    label?: string;
}

export const TextInput = ({
    error,
    label,
    className,
    ...props
}: TextInputProps) => {
    return (
        <div>
            <label key={label} className={clsx('flex', 'flex-col')}>
                {label && <span className={clsx('text-start')}>{label}</span>}
                <input
                    key={error}
                    type="text"
                    className={clsx(
                        {
                            'border-red border-solid': error,
                            'border-black border-solid': !error,
                        },
                        'border-2',
                        'rounded-md',
                        className
                    )}
                    {...props}
                />
                {error && (
                    <span className={clsx('text-red', 'text-start')}>
                        {error}
                    </span>
                )}
            </label>
        </div>
    );
};
