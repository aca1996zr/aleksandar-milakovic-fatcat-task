import { ReactNode } from 'react';

import { clsx } from 'clsx';

export type LayoutProps = {
    children: ReactNode;
    background?: string;
    title?: string;
};

export const Layout = ({ children, background, title }: LayoutProps) => {
    return (
        <section className={clsx('py-20', background)}>
            <span
                className={clsx(
                    'text-bold',
                    'text-3xl',
                    'text-center',
                    'flex',
                    'justify-center'
                )}
                key={title}
            >
                {title}
            </span>
            {children}
        </section>
    );
};
