import { Item } from '@homework-task/models/data';

import { Hero } from '../Hero';
import { ItemsShowcase } from '../ItemsShowcase';
import { Layout } from '../Layout';
import { TrustBar } from '../TrustBar';

const componentFactory = {
    componentTrustBar: TrustBar,
    componentItemsShowcase: ItemsShowcase,
    layoutSection: Layout,
    componentHero: Hero,
};

type FactoryType = typeof componentFactory;

type ComponentType<T extends FactoryType, K = keyof T> = {
    type: K;
    props?: AllComponentProps;
};

export type SectionType = ComponentType<FactoryType> & {
    components: Array<ComponentType<FactoryType>>;
};

type GeneratorProps = {
    data: Array<SectionType>;
};

type AllComponentProps = React.ComponentProps<typeof TrustBar> &
    React.ComponentProps<typeof ItemsShowcase> &
    React.ComponentProps<typeof Layout> &
    React.ComponentProps<typeof Hero>;
export const Generator = ({ data }: GeneratorProps) => {
    return (
        <div>
            {data.map(({ type, components, props }, index) => {
                const Layout = componentFactory[type];

                const children = components.map(({ type, props }, index) => {
                    const Component = componentFactory[type];

                    return (
                        <Component
                            images={props?.images as string[]}
                            items={props?.items as Item[]}
                            title={props?.title as string}
                            key={`${type}-${index}`}
                            {...props}
                        >
                            {props?.children}
                        </Component>
                    );
                });

                return (
                    <Layout
                        images={props?.images as string[]}
                        items={props?.items as Item[]}
                        title={props?.title as string}
                        key={`${type}-${index}`}
                        {...props}
                    >
                        {children}
                    </Layout>
                );
            })}
        </div>
    );
};
