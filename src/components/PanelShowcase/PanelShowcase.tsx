import { clsx } from 'clsx';

import { Item } from '@homework-task/models/data';

type PanelShowcaseProps = {
    items: Item[];
};

export const PanelShowcase = ({ items }: PanelShowcaseProps) => {
    return (
        <div className={clsx('flex', 'justify-center', 'items-center')}>
            <div
                key={'wrapper'}
                className={clsx(
                    'grid',
                    'grid-cols-3',
                    'gap-8',
                    'w-8/12',
                    'justify-center',
                    'items-center'
                )}
            >
                {items.map(({ title, description, image }) => (
                    <div
                        key={title}
                        className={clsx(
                            'flex',
                            'flex-col',
                            'gap-2',
                            'justify-center',
                            ' items-center',
                            'text-center'
                        )}
                    >
                        {image && (
                            <img
                                src={image}
                                width="50"
                                height="50"
                                alt="Icon"
                            />
                        )}
                        <div
                            key={title}
                            className={clsx('text-xl', 'font-bold')}
                        >
                            {title}
                        </div>
                        <div key={description}>{description}</div>
                    </div>
                ))}
            </div>
        </div>
    );
};
