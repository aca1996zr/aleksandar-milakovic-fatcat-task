import { Item } from '@homework-task/models/data';

export type HeroProps = Omit<Item, 'description'>;

export const Hero = ({ title, image }: HeroProps) => {
    return (
        <div className="flex row justify-center items-center gap-4 h-screen">
            <div key={`${title}-wrapper`} className="w-4/12">
                <h1 key={title} className="text-3xl font-bold">
                    {title}
                </h1>
            </div>
            <div key={`${image}-warpper`} className="w-4/12">
                <img
                    key={image}
                    src={image}
                    width="100%"
                    alt="Hero"
                    loading="eager"
                />
            </div>
        </div>
    );
};
