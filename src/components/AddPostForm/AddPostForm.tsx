import { clsx } from 'clsx';

import { CreateForm } from '@homework-task/components';
import { useAddPost } from '@homework-task/hooks/api/useAddPost';
import { FormValidatorSchema } from '@homework-task/schemas/FormValidator.schema';

import { ControlledTextArea } from '../ControlledTextArea';
import { ControlledTextInput } from '../ControlledTextInput';

export const AddPostForm = () => {
    const { mutate: addPost } = useAddPost({
        onSuccess: () => {},
        onError: () => {},
    });
    return (
        <div
            key={'form'}
            className={clsx(
                'flex',
                'flex-col',
                'justify-center',
                'items-center',
                'bg-cream',
                'mt-4',
                'gap-8'
            )}
        >
            <span key={'header'} className={clsx('text-2xl')}>
                Form
            </span>
            <CreateForm
                key={'create-form'}
                apiHookCall={addPost}
                validationSchema={FormValidatorSchema}
                renderForm={() => {
                    return (
                        <div className={clsx('gap-8', 'flex', 'flex-col')}>
                            <ControlledTextInput
                                placeholder="Title"
                                name="title"
                                label="Label"
                                key={'controlledTextInput'}
                            />

                            <ControlledTextArea
                                key={'controlledTextArea'}
                                placeholder="Body"
                                name="body"
                                label="Body Label"
                            />
                        </div>
                    );
                }}
            />
        </div>
    );
};
