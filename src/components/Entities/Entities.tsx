import React, { ReactNode } from 'react';

import { clsx } from 'clsx';
import { z } from 'zod';

import { useEntities } from '@homework-task/hooks/api/useEntities';

type EntitiesProps = {
    urlExtension: string;
    schema: z.ZodArray<z.AnyZodObject>;
};

const getObjectKeys = (obj?: Record<string, unknown>) => {
    return obj ? Object.keys(obj) : [];
};

const renderTableHeaders = (data?: Record<string, unknown>): ReactNode => {
    if (!data) return null;

    const headers = getObjectKeys(data);
    return (
        <>
            <tr>
                {headers.map((header) => (
                    <th
                        className={clsx(
                            'px-4 py-2 bg-blue-200 text-blue-800 border border-blue-300'
                        )}
                        key={header}
                    >
                        {header}
                    </th>
                ))}
            </tr>
            <tr>
                {headers.map((header) =>
                    typeof data[header] === 'object' ? (
                        getObjectKeys(
                            data[header] as Record<string, unknown>
                        ).map((childKey) => (
                            <th key={`${header}-${childKey}`}>{childKey}</th>
                        ))
                    ) : (
                        <th key={header}></th>
                    )
                )}
            </tr>
        </>
    );
};

const renderTableCells = (
    dataArray: Record<string, unknown>[] | undefined,
    headers: string[]
): ReactNode => {
    return dataArray?.map((dataItem, index) => (
        <tr
            key={index}
            className={clsx(
                index % 2 === 0 ? 'bg-cream' : 'bg-white',
                'hover:bg-gray-200'
            )}
        >
            {headers.map((header) => {
                const cellData = dataItem[header];
                if (typeof cellData !== 'object') {
                    return (
                        <td
                            className={clsx(
                                'border border-gray-300 px-4 py-2 text-gray-600'
                            )}
                            key={header}
                        >
                            <>{cellData ?? ''}</>
                        </td>
                    );
                }

                return (
                    <td key={index}>
                        {renderTableCells(
                            [cellData as Record<string, unknown>],
                            getObjectKeys(cellData as Record<string, unknown>)
                        )}
                    </td>
                );
            })}
        </tr>
    ));
};

export const Entities: React.FC<EntitiesProps> = ({ urlExtension, schema }) => {
    const { data, isLoading, isError } = useEntities(urlExtension, schema);

    if (isLoading) return <div>Loading...</div>;
    if (isError) return <div>Error loading data</div>;

    const headers = getObjectKeys(data?.[0]);
    return (
        <div>
            <table
                key={'table'}
                className={clsx('min-w-full table-auto border-collapse')}
            >
                <thead key={'thead'} className={clsx('bg-gray-200')}>
                    {renderTableHeaders(data?.[0])}
                </thead>
                <tbody key={'tbody'}>{renderTableCells(data, headers)}</tbody>
            </table>
        </div>
    );
};
