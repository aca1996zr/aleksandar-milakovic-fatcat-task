import React from 'react';

import { zodResolver } from '@hookform/resolvers/zod';
import { UseMutateFunction } from '@tanstack/react-query';
import { clsx } from 'clsx';
import { useForm, FormProvider } from 'react-hook-form';
import { z } from 'zod';

import { AddPostRequestParams } from '@homework-task/services/api/addPost';

interface CustomControllerProps {
    renderForm: () => React.ReactNode;
    validationSchema: z.Schema;
    apiHookCall: UseMutateFunction<
        unknown,
        Error,
        AddPostRequestParams,
        unknown
    >;
}

export const CreateForm = ({
    renderForm,
    validationSchema,
    apiHookCall,
}: CustomControllerProps) => {
    const methods = useForm({
        resolver: zodResolver(validationSchema),
        mode: 'all',
    });
    const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        methods
            .trigger()
            .then((isFormValid) => {
                if (isFormValid) {
                    return apiHookCall({ formData: methods.getValues() });
                }
            })
            .catch(() => {
                return;
            });
    };
    return (
        <FormProvider {...methods}>
            <form
                key={'s'}
                onSubmit={onSubmit}
                className={clsx(
                    'flex',
                    'flex-col',
                    'justify-center',
                    'gap-2',
                    'width-50',
                    'w-2/4',
                    'ml-auto',
                    'mr-auto',
                    'text-center',
                    'gap-8'
                )}
            >
                {renderForm()}
                <button
                    key={'submit'}
                    type="submit"
                    className={clsx('bg-tertiaryYellow', 'mb-4')}
                >
                    SUBMIT
                </button>
            </form>
        </FormProvider>
    );
};
