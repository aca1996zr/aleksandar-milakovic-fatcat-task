import { ComponentProps } from 'react';

import { clsx } from 'clsx';

export interface TextAreaProps extends ComponentProps<'textarea'> {
    error?: string;
    label?: string;
}

export const TextArea = ({
    error,
    label,
    className,
    ...props
}: TextAreaProps) => {
    return (
        <div>
            <label key={label} className={clsx('flex', 'flex-col')}>
                {label && <span className={clsx('text-start')}>{label}</span>}

                <textarea
                    key={props.name}
                    className={clsx(
                        {
                            'border-red border-solid': error,
                            'border-black border-solid': !error,
                        },
                        'rounded-md',

                        'border-2',
                        className
                    )}
                    {...props}
                />
                {error && (
                    <span className={clsx('text-red', 'text-start')}>
                        {error}
                    </span>
                )}
            </label>
        </div>
    );
};
