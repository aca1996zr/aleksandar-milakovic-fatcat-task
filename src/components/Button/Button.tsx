import { ComponentProps } from 'react';

import { clsx } from 'clsx';

interface ButtonProps extends ComponentProps<'button'> {}

export const Button = ({ onClick, className, children }: ButtonProps) => {
    return (
        <button
            className={clsx(
                'rounded-lg',
                'px-4',
                'py-2',
                'bg-black',
                'text-white',
                className
            )}
            onClick={onClick}
        >
            {children}
        </button>
    );
};
