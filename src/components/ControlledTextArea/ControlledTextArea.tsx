import { useFormContext, Controller } from 'react-hook-form';

import { TextArea, TextAreaProps } from '../TextArea';

interface ConterolledTextAreaProps extends TextAreaProps {
    name: string;
}

export const ControlledTextArea = ({
    name,
    ref,
    ...props
}: ConterolledTextAreaProps) => {
    const { control } = useFormContext();

    return (
        <Controller
            name={name}
            defaultValue={''}
            control={control}
            render={({ field, fieldState }) => {
                const error = fieldState.error?.message;
                return (
                    <TextArea error={error} {...props} {...field} ref={ref} />
                );
            }}
        />
    );
};
