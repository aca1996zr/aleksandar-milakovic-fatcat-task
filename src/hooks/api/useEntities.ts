import { UseQueryOptions, useQuery } from '@tanstack/react-query';
import { z } from 'zod';

import { getEntities } from '@homework-task/services/api/getEntities';

type UseUsersOptions = Omit<
    UseQueryOptions<unknown, Error, Array<Record<string, unknown>>>,
    'queryKey' | 'queryFn'
>;

export const useEntities = (
    key: string,
    schema: z.ZodSchema,
    queryOptions?: UseUsersOptions
) => {
    return useQuery({
        queryKey: [key],
        queryFn: () => getEntities(key, schema),
        ...queryOptions,
    });
};
