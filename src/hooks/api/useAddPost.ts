import { useMutation, UseMutationOptions } from '@tanstack/react-query';

import {
    addPost,
    AddPostRequestParams,
} from '@homework-task/services/api/addPost';

type UseAddPostOptions = Omit<
    UseMutationOptions<unknown, Error, AddPostRequestParams, unknown>,
    'mutationKey' | 'mutationFn'
>;

export function useAddPost(mutationOptions?: UseAddPostOptions) {
    return useMutation({
        mutationKey: ['add-post'],
        mutationFn: addPost,
        ...mutationOptions,
    });
}
