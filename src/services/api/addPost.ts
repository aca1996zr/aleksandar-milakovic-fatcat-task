import { axiosRequest } from '../axiosConfig';

export interface AddPostRequestParams {
    formData: unknown;
}

export const addPost = async ({
    formData,
}: AddPostRequestParams): Promise<unknown> => {
    const { data } = await axiosRequest<unknown>('POST', `/posts`, {
        data: {
            formData,
        },
    });
    return data;
};
