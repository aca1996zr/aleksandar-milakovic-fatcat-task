import { z } from 'zod';

import { axiosRequest } from '@homework-task/services/axiosConfig';

export const getEntities = async (
    key: string,
    schema: z.Schema
): Promise<unknown> => {
    const { data } = await axiosRequest<unknown>('GET', `/${key}`);
    const parsedData = schema.parse(data) as unknown;
    return parsedData;
};
