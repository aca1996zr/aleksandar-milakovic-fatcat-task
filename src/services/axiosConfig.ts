import axios, { AxiosPromise, AxiosRequestConfig } from 'axios';

const BASE_URL = 'https://jsonplaceholder.typicode.com/';

export const axiosRequest = <T = unknown>(
    method: 'POST' | 'GET',
    path: string,
    config?: Omit<AxiosRequestConfig, 'method' | 'url' | 'baseURL'>
): AxiosPromise<T> => {
    const axiosConfig: AxiosRequestConfig = {
        url: BASE_URL + path,
        method: method,
        ...config,
    };

    return axios(axiosConfig);
};
