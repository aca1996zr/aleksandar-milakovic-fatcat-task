### To Run the Project

npm install
npm run dev

### Create List Component

Solution for this is presented in 'Entities' component.

The Entities component is a data-driven table renderer. It is designed to dynamically display data fetched from an API endpoint. The component uses a flexible structure that allows it to handle various data shapes and schemas, making it highly reusable for different data visualization needs.

Features
*Dynamic Data Rendering: Automatically renders a table based on the provided data, adapting to different data structures.
*Customizable Table Headers: Dynamically generates table headers based on the keys present in the data.
// I am aware that this solution is not fully dynamic and can be expanded

### Create a Form Generator Component

Solution for this is written in 'CreateForm' component.
The 'CreateForm' component is a dynamic and versatile form builder designed for creating and handling forms.
It offers flexibility in form layout, validation with Zod, and form submission handling.

Features
*Dynamic Form Rendering: Allows for the injection of various form fields through the renderForm prop.
*Form Validation: Integrates with Zod for defining and applying validation rules to form data.
*React Hook Form Integration: Utilizes react-hook-form for efficient form state management, input handling, and submission.
*Customizable Form Submission: Employs an apiHookCall function, passed via props, for handling form submission logic, such as API calls.

### Create a Page Generator Component

Solution for this is written in 'Generator' component.

The 'Generator' component serves as a dynamic layout builder, capable of rendering a complex and hierarchical structure of UI components based on provided configuration data. It uses a component factory pattern to create different UI elements dynamically, making it highly adaptable for various layout needs.

Features
*Dynamic Component Creation: Utilizes a componentFactory to instantiate different components like Hero, ItemsShowcase, Layout, and TrustBar based on the type specified in the provided data.
*Configurable and Nested Layouts: Each component can be configured with specific props and can also contain nested components, allowing for the creation of deeply nested and rich UI structures.
